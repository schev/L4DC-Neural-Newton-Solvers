import numpy as np

def RK_ft(dt,f,x1,k1,k2):
    x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
    ft = k2 - f(x2)
    return ft

def RK_Jt(dt,J,x1,k1,k2,n):
    x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
    Jt = np.identity(n)-0.5*dt*J(x2)
    
    # Original (mistake)
    # Jt = np.identity(n)-0.5*dt*J(k2)
    
    return Jt
