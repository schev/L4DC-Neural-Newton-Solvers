# Test Simulation
import numpy as np
import copy
import matplotlib.pyplot as plt
import sys
import math
import pickle
import os

# Change working directory to the location of this file
os.chdir(os.path.dirname(__file__))  # Test: wd=os.getcwd()

# %% Add functions to path
sys.path.append("..\Functions")

# ----- Alternative:
# project_directory = r'C:\Users\Samuel.HORACE\Dropbox (Personal)\Documents\Python\L4DC'
# sys.path.append(project_directory + '\Functions')
# i.e., sys.path.append(r'C:\Users\Samuel.HORACE\Dropbox (Personal)\Documents\Python\L4DC\Functions')


# Define Random Seed
np.random.seed(1)

from RK_Trap_fJ               import RK_ft, RK_Jt
from RK_Trap_Sim              import RKT_Sim
from Projected_Newton_Step    import Projected_Newton_Step

# %% Define System
system = 'K' # 'C' --or-- 'H' --or-- 'K'

# Cubic Oscillator
if system == 'C':

    # System Dynamics
    from Cubic_Oscillator import f, J
    x0 = np.array([0.8, -0.4])
    
    # Simulation Conditions
    dt       = 0.025
    t0       = 0
    tf       = 45
    Newt_Tol = 1e-9
    Eu_step  = 0
    num_sims = 50
    
    # Data File
    Data_File = "Cubic_Data.pkl"
    
# Hopf Oscillator
elif system == 'H':
    
    # System Dynamics
    from Hopf import f, J
    x0 = np.array([0.1, -0.5, -0.5])
    
    # Simulation Conditions
    dt       = 0.025
    t0       = 0
    tf       = 45
    Newt_Tol = 1e-9
    Eu_step  = 0
    num_sims = 50
    
    # Data File
    Data_File = "Hopf_Data.pkl"
    
# Kundur
else: # (system == 'K')
    
    # System Dynamics
    from Kundur import f, J, x0f
    x0 = x0f().flatten()
    x0[0:4]  = x0[0:4]  + 2*math.pi
    x0[8:10] = x0[8:10] + 2*math.pi

    # Simulation Conditions
    dt       = 0.001
    t0       = 0
    tf       = 2.5
    Newt_Tol = 1e-9
    Eu_step  = 0
    num_sims = 50
    
    # Data File
    Data_File = "Kundur_Data.pkl"

# %% Run Simulation -- Save Projection Data
for ii in range(num_sims):
    print(ii)
    
    # Perturb initial conditions
    x0p = copy.copy(x0)
    
    if system == 'C':
        x0p = x0p + 0.25*np.random.randn(2)
    elif system == 'H':
        x0p = x0p + 0.25*np.random.randn(3)
    else:
        x0p[0:4] = x0p[0:4] + 0.02*np.random.randn(4)
        
    # Simualte and collect data
    data, NewtStps_Param, NewtStps_In, NewtStps_Out, NewtFuncs, NewtProj = RKT_Sim(x0p,dt,t0,tf,f,J,RK_ft,RK_Jt,Projected_Newton_Step,Newt_Tol,Eu_step)
    
    # Plot the data
    plt.plot(data.T)
    
    # Aggregate
    if ii == 0:
        Data_Sim    = copy.copy(data)
        Data_SimIn  = [len(data[0,:])]
        Data_ParamX = copy.copy(NewtStps_Param)
        Data_ParamK = copy.copy(NewtStps_In)
        Data_In     = copy.copy(NewtFuncs)
        Data_Out    = copy.copy(NewtProj)
    else:
        Data_Sim    = np.concatenate((Data_Sim,data),1)
        Data_SimIn  = np.concatenate((Data_SimIn,[len(data[0,:])]))
        Data_ParamX = np.concatenate((Data_ParamX,NewtStps_Param),1)
        Data_ParamK = np.concatenate((Data_ParamK,NewtStps_In),1)
        Data_In     = np.concatenate((Data_In,NewtFuncs),1)
        Data_Out    = np.concatenate((Data_Out,NewtProj),1)

# Now, throw all of the data into a list, and then export as a pickle
Master_List = [Data_Sim, Data_SimIn, Data_ParamX, Data_ParamK, Data_In, Data_Out]
plt.show()

# Save Data
with open(Data_File, 'wb') as f:
        pickle.dump(Master_List, f)
