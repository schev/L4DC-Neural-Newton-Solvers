import numpy as np
import copy

from pytictoc import TicToc

def RK_Trap_NN_Sims(x0p,dt,t0,tf,f,J,RK_ft,RK_Jt,Eu_step,Newt_Tol,Step_Tol,W0,W1,W2,W3,b0,b1,b2,b3,In_Mean,In_STD,Out_Mean,Out_STD):
    tspan     = np.arange(t0,tf,dt)
    n         = len(x0p)
    Xv        = np.empty([n,len(tspan)+1])
    Xv[:,0]   = x0p
    XvNN      = np.empty([n,len(tspan)+1])
    XvNN[:,0] = x0p
    ii        = 0  # count time steps
    iiNN      = 0  # count time steps
    II        = np.identity(n)
    
    # Start Timing Function
    tt = TicToc()
    Time_Newton = np.empty(len(tspan))
    Time_NN     = np.empty(len(tspan))
       
    # Loop
    jj   = 0
    jjNN = 0
    for t in tspan:
        
        #######################################################
        #######################################################
        ############   1) Use Newton to Solve RK   ############
        #######################################################
        #######################################################
        #
        # Solve Newton for this time step
        tt.tic();
        x1 = Xv[:,ii]
        k1 = f(x1)
        if Eu_step == 1:
            # Initial Guess: Euler
            k2 = f(x1+dt*k1)
        else:
            # Cold start: useful for training
            k2   = copy.copy(k1)
            x2   = x1 + 0.5*dt*k1 + 0.5*dt*k2
            fn   = k2 - f(x2)

        while abs(max(fn, key=abs)) > Newt_Tol:
            # Define Jacobian and Function Values
            #tt.tic();
            jj += 1
            x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
            Jn = II - 0.5*dt*J(x2)
            
            #Jn = II - 0.5*dt*np.matrix([[-0.3*(x2[0]**2), 6*(x2[1]**2)],
                            #[-6*(x2[0]**2), -0.3*(x2[1]**2)]])
            #
            # Alternatives
            # fn = RK_ft(dt,f,x1,k1,k2)
            # Jn = RK_Jt(dt,J,x1,k1,k2,n)
            
            # Newton Step
            #tt.tic();
            k2 = k2 - np.linalg.solve(Jn, fn)
            x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
            fn = k2 - f(x2)
            # fn = k2 - np.array([-0.1*(x2[0]**3) + 2*(x2[1]**3),-2*(x2[0]**3) - 0.1*(x2[1]**3)])

            
        # Now, compute new state & concatenate
        ii += 1
        x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
        Xv[:,ii] = x2
        
        # Update the time
        Time_Newton[ii-1] = tt.tocvalue();
        
        #######################################################
        #######################################################
        ############     2) Use NN to Solve RK     ############
        #######################################################
        #######################################################
        #
        # Solve Newton for this time step
        tt.tic();
        x1NN = XvNN[:,iiNN]
        k1NN = f(x1NN)
        if Eu_step == 1:
            # Initial Guess: Euler
            k2NN = f(x1NN+dt*k1NN)
        else:
            # Cold start: useful for training
            k2NN = k1NN

        k20NN = 100*copy.copy(k2NN)
        while abs(max(k20NN-k2NN, key=abs)) > Step_Tol:
        # while abs(max(RK_ft(dt,f,x1NN,k1NN,k2NN), key=abs)) > Newt_Tol:
            # Prepared NN data
            jjNN     += 1
            k20NN     = copy.copy(k2NN)
            f_eval    = k2NN - f(x1NN + 0.5*dt*k1NN + 0.5*dt*k2NN)
            #
            # Alternative:
            # f_eval    = RK_ft(dt,f,x1NN,k1NN,k20NN)
            
            # 1. Normalize function
            Norm_f       = np.linalg.norm(f_eval)
            f_Normalized = f_eval/Norm_f
            
            # 2. Normalize inputs
            NNData_In            = np.concatenate((f_Normalized,x1NN,k20NN),0)
            NNData_In_Normalized = (NNData_In - In_Mean)/In_STD
            
            # 3. Pass to NN
            NN_out_Normalized = W3.dot(np.maximum(W2.dot(np.maximum(W1.dot(np.maximum(W0.dot(NNData_In_Normalized) + b0,0)) + b1,0)) + b2,0)) + b3

            # 4. Un-Normalize onputs
            NN_out       = (NN_out_Normalized*Out_STD)+Out_Mean
            
            # 5. Normalize Step
            Newton_step  = Norm_f*NN_out

            # Take a step
            k2NN      = k20NN - Newton_step
            
        # Now, compute new state & concatenate
        iiNN += 1
        x2NN         = x1NN + 0.5*dt*k1NN + 0.5*dt*k2NN
        XvNN[:,iiNN] = x2NN
        
        # Update the time
        Time_NN[iiNN-1] = tt.tocvalue();
      
    print(jj)
    print(jjNN)
        
    return Xv, XvNN, Time_Newton, Time_NN
