import numpy as np

def Projected_Newton_Step(Jn,fn,x,alpha):
    proj = np.linalg.solve(Jn, fn)
    xnew = x - alpha*proj
    return xnew, proj
