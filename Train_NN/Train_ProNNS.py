import matplotlib.pyplot as plt
import torch
import pickle
import numpy as np
import torch.nn as nn
import os

from torch.utils.data import DataLoader, TensorDataset

# Change working directory to the location of this file
os.chdir(os.path.dirname(__file__))  # Test: wd=os.getcwd()

# %% Define System
system = 'K' # 'C' --or-- 'H' --or-- 'K'

# Cubic Oscillator
if system == 'C':
    num_sv      = 2
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 10
    Data_File   = "Cubic_Data.pkl"
    NN_File     = "Cubic_NN_Model.pkl"
    Normalization_File = "Cubic_Normalize_Data.pkl"
    
# Hopf Oscillator
elif system == 'H':
    num_sv      = 3
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 10
    Data_File   = "Hopf_Data.pkl"
    NN_File     = "Hopf_NN_Model.pkl"
    Normalization_File = "Hopf_Normalize_Data.pkl"
    
# Kundur
else: # (system == 'K')
    num_sv      = 10
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 20
    Data_File   = "Kundur_Data.pkl"
    NN_File     = "Kundur_NN_Model.pkl"
    Normalization_File = "Kundur_Normalize_Data.pkl"
    
# Define 3 NN Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size):
        super(ReluInput, self).__init__()
        self.linear = nn.Linear(Input_size, W_size)
        self.ReLU = nn.ReLU()

    def forward(self, x):
        return self.ReLU(self.linear(x)) # Apply ReLU and Return

class ReluLayer(torch.nn.Module):
    def __init__(self, W_size):
        super(ReluLayer, self).__init__()
        self.linear = nn.Linear(W_size, W_size)
        self.ReLU = nn.ReLU()

    def forward(self, x):
        return self.ReLU(self.linear(x)) # Apply ReLU and Return

class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size):
        super(ReluOutput, self).__init__()
        self.linear = nn.Linear(W_size, Output_size)

    def forward(self, x):
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Network
net = torch.nn.Sequential(ReluInput(Input_size,NN_width),
                          ReluLayer(NN_width),
                          ReluLayer(NN_width),
                          ReluOutput(Output_size,NN_width))

# Load and parse system
PATH = "..\Collect_Data\\" + Data_File
with open(PATH, 'rb') as f:
    training_data = pickle.load(f)

# Data
Data_Sim    = training_data[0]
Data_SimIn  = training_data[1] 
Data_ParamX = training_data[2]
Data_ParamK = training_data[3]
Data_In     = training_data[4]
Data_Out    = training_data[5]

# Concatenate, transpose, and prepare the data
NNData_In  = torch.from_numpy(np.concatenate((Data_In,Data_ParamX,Data_ParamK),0).transpose()).float()
NNData_Out = torch.from_numpy(Data_Out.transpose()).float()

# NORMALIZATION -- Part 1: fn = fn / ||fn||
ii = 0
for data_row in NNData_In:
    f_norm                 = np.linalg.norm(data_row[0:num_sv])
    NNData_In[ii,0:num_sv] = NNData_In[ii,:][0:num_sv]/f_norm
    NNData_Out[ii,:]       = NNData_Out[ii,:]/f_norm
    ii += 1
    
# NORMALIZATION -- Part 2: Input/Output Transformation
In_Mean  = np.zeros(NNData_In.shape[1])
In_STD   = np.zeros(NNData_In.shape[1])
Out_Mean = np.zeros(NNData_Out.shape[1])
Out_STD  = np.zeros(NNData_Out.shape[1])

for ii in range(NNData_In.shape[1]):
    # Transform Input
    if (system == 'H') and ((ii == 0) or (ii == 6)):
        In_Mean[ii]     = 0
        In_STD[ii]      = 1
        NNData_In[:,ii] = NNData_In[:,ii]
    else:
        In_Mean[ii]     = torch.mean(NNData_In[:,ii]).item()
        In_STD[ii]      = torch.std(NNData_In[:,ii]).item()
        NNData_In[:,ii] = (NNData_In[:,ii] - In_Mean[ii])/In_STD[ii]  

for ii in range(NNData_Out.shape[1]):
    # Transform Output
    if (system == 'H') and (ii == 0):
        Out_Mean[ii]     = 0
        Out_STD[ii]      = 1
        NNData_Out[:,ii] = NNData_Out[:,ii] - Out_Mean[ii]
    else:
        Out_Mean[ii]     = torch.mean(NNData_Out[:,ii]).item()
        Out_STD[ii]      = torch.std(NNData_Out[:,ii]).item()
        NNData_Out[:,ii] = (NNData_Out[:,ii] - Out_Mean[ii])/Out_STD[ii]

# %% Load Network or Train From Scratch
Load_Network = 0

if Load_Network == 1:
    PATH = ".\\" + NN_File
    net.load_state_dict(torch.load(PATH))

# %% Stage 1 Optimization: Standard MSE
it_cnt = 5000
opt    = torch.optim.Adam(net.parameters(), lr=1e-4)
error_vec = np.empty([it_cnt])
for kk in range(it_cnt):
    opt.zero_grad()
    l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
    print(kk)
    print (l.item())
    error_vec[kk] = l.item()
    l.backward()
    opt.step()
    
# %% Stage 2 Optimization: Data Loader
it_cnt     = 2500
batch      = 256
opt        = torch.optim.Adam(net.parameters(), lr=1e-5)
dataset    = TensorDataset(NNData_In, NNData_Out)
dataloader = DataLoader(dataset, batch_size = batch, shuffle=True)

for it_cnt in range(it_cnt):
    # Loop over batches in an epoch using DataLoader
    for id_batch, (x_batch, y_batch) in enumerate(dataloader):
        opt.zero_grad()
        l = torch.nn.MSELoss()(net(x_batch), y_batch)
        l.backward()
        opt.step()
    
    # Compute the loss
    l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
    print (l.item())

# %% Save the NN model
PATH = ".\\" + NN_File
torch.save(net.state_dict(),PATH)

# Save the Normalization Data
Normalization_List = [In_Mean, In_STD, Out_Mean, Out_STD]
PATH               = ".\\" + Normalization_File
with open(PATH, 'wb') as f:
        pickle.dump(Normalization_List, f)

# %% Plot Results
fig, axs = plt.subplots(2)
axs[0].plot(net(NNData_In).detach().numpy(),color='orange')
axs[0].plot(NNData_Out.detach().numpy(),color='blue')
axs[1].plot(error_vec)
plt.show()