import matplotlib.pyplot as plt
import torch
import pickle
import copy
import numpy as np
import sys
import os
import torch.nn as nn
import math

# Change working directory to the location of this file
os.chdir(os.path.dirname(__file__))  # Test: wd=os.getcwd()

# %% Add functions to path
sys.path.append("..\Functions")

# ----- Alternative:
# project_directory = r'C:\Users\Samuel.HORACE\Dropbox (Personal)\Documents\Python\L4DC'
# sys.path.append(project_directory + '\Functions')
# i.e., sys.path.append(r'C:\Users\Samuel.HORACE\Dropbox (Personal)\Documents\Python\L4DC\Functions')

# Define Random Seed
np.random.seed(1)

from RK_Trap_NN_Sims          import RK_Trap_NN_Sims
from RK_Trap_fJ               import RK_ft, RK_Jt

# %% Define System
system = 'K' # 'C' --or-- 'H' --or-- 'K'

# Cubic Oscillator
if system == 'C':
    
    # System Dynamics
    from Cubic_Oscillator import f, J
    x0 = np.array([0.8, -0.4])
    
    # Simulation Conditions
    dt       = 0.025
    t0       = 0
    tf       = 65
    Newt_Tol = 1e-9
    Eu_step  = 0
    
    # NN Data
    num_sv      = 2
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 10
    NN_File     = "Cubic_NN_Model.pkl"
    Normalization_File = "Cubic_Normalize_Data.pkl"
    
# Hopf Oscillator
elif system == 'H':
    
    # System Dynamics
    from Hopf import f, J
    x0 = np.array([0.1, -0.5, -0.5])
    
    # Simulation Conditions
    dt       = 0.025
    t0       = 0
    tf       = 45
    Newt_Tol = 1e-9
    Eu_step  = 0
    
    # NN Data
    num_sv      = 3
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 10
    NN_File     = "Hopf_NN_Model.pkl"
    Normalization_File = "Hopf_Normalize_Data.pkl"
    
# Kundur
else: # (system == 'K')

    # System Dynamics
    from Kundur import f, J, x0f
    x0 = x0f().flatten()
    x0[0:4]  = x0[0:4]  + 2*math.pi
    x0[8:10] = x0[8:10] + 2*math.pi

    # Simulation Conditions
    dt       = 0.001
    t0       = 0
    tf       = 2.5
    Newt_Tol = 1e-9
    Eu_step  = 0

    # NN Data
    num_sv      = 10
    Input_size  = 3*num_sv
    Output_size = num_sv
    NN_width    = 20
    NN_File     = "Kundur_NN_Model.pkl"
    Normalization_File = "Kundur_Normalize_Data.pkl"

# Define 3 NN Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size):
        super(ReluInput, self).__init__()
        self.linear = nn.Linear(Input_size, W_size)
        self.ReLU = nn.ReLU()

    def forward(self, x):
        return self.ReLU(self.linear(x)) # Apply ReLU and Return

class ReluLayer(torch.nn.Module):
    def __init__(self, W_size):
        super(ReluLayer, self).__init__()
        self.linear = nn.Linear(W_size, W_size)
        self.ReLU = nn.ReLU()

    def forward(self, x):
        return self.ReLU(self.linear(x)) # Apply ReLU and Return

class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size):
        super(ReluOutput, self).__init__()
        self.linear = nn.Linear(W_size, Output_size)

    def forward(self, x):
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Network
net = torch.nn.Sequential(ReluInput(Input_size,NN_width),
                          ReluLayer(NN_width),
                          ReluLayer(NN_width),
                          ReluOutput(Output_size,NN_width))

# Load the NN parameters
PATH = "..\Train_NN\\" + NN_File
net.load_state_dict(torch.load(PATH))
net.double()
net.eval()

# Parse the NN parameters
W0 = net[0].linear.weight.detach().numpy()
W1 = net[1].linear.weight.detach().numpy()
W2 = net[2].linear.weight.detach().numpy()
W3 = net[3].linear.weight.detach().numpy()
b0 = net[0].linear.bias.detach().numpy()
b1 = net[1].linear.bias.detach().numpy()
b2 = net[2].linear.bias.detach().numpy()
b3 = net[3].linear.bias.detach().numpy()

# Load in Normalization Data
PATH = "..\Train_NN\\" + Normalization_File
with open(PATH, 'rb') as nf:
    normalization_data = pickle.load(nf)

# Data
In_Mean  = normalization_data[0]
In_STD   = normalization_data[1]
Out_Mean = normalization_data[2]
Out_STD  = normalization_data[3]

# %% Test Simulation
np.random.seed(3)
Step_Tol = 1e-2

# Loop over tests
for ii in range(5):
    print(ii)
    
    # Perturb initial conditions
    x0p = copy.copy(x0)
    
    if system == 'C':
        x0p = x0p + 0.25*np.random.randn(2)
    elif system == 'H':
        x0p = x0p + 0.25*np.random.randn(3)
    else:
        x0p[0:4] = x0p[0:4] + 0.02*np.random.randn(4)
        
    # Simulate
    Xv, XvNN, Time_Newton, Time_NN = RK_Trap_NN_Sims(x0p,dt,t0,tf,f,J,RK_ft,RK_Jt,Eu_step,Newt_Tol,Step_Tol,W0,W1,W2,W3,b0,b1,b2,b3,In_Mean,In_STD,Out_Mean,Out_STD)
    
    # Plot results
    if ii == 0:
             plt.plot(Xv[1,:],color='orange')
             plt.plot(XvNN[1,:],'b--')
             plt.legend(['Newton','Neural Network'])
    plt.plot(Xv.T,color='orange')
    plt.plot(XvNN.T,'b--')

    #plt.plot(Time_Newton)
    #plt.plot(Time_NN)
    #plt.legend(['Time: Trapezoidal Step via Newton','Time: Trapezoidal Step via Neural Network'])
    
plt.show()
